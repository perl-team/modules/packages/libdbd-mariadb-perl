Source: libdbd-mariadb-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-libmysqlclient-dev,
               default-mysql-server <!nocheck>,
               dh-sequence-perl-dbi,
               libdbi-perl,
               libdevel-checklib-perl,
               libnet-ssleay-perl <!nocheck>,
               libssl-dev,
               libtest-deep-perl <!nocheck>,
               perl-xs-dev,
               perl:native,
               procps <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdbd-mariadb-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdbd-mariadb-perl.git
Homepage: https://metacpan.org/release/DBD-MariaDB
Rules-Requires-Root: no

Package: libdbd-mariadb-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libdbi-perl
Description: Perl5 database interface to the MariaDB/MySQL databases
 DBD::MariaDB is a Perl5 Database Interface driver for the MariaDB/MySQL
 databases. In other words: DBD::MariaDB is an interface between the Perl
 programming language and the MariaDB/MySQL programming API that comes with
 the MariaDB/MySQL relational database management systems. Most functions
 provided by this programming API are supported.
 .
 DBD::MariaDB is a fork of DBD::mysql (libdbd-mysql-perl) with inprovements in
 MariaDB compatibility, Perl Unicode support, and some security related bug
 fixes, at the cost of not being 100% backwards compatible.
